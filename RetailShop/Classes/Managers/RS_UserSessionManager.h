//
//  RS_UserSessionManager.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RS_User;
@class RS_ShoppingCart;
@class RS_Product;


extern NSString *const RS_UserSessionManagerCartDidUpdateNotification;

/**
 *  Manager class for users shopping sessions
 */

@interface RS_UserSessionManager : NSObject

/**
 *  current active user
 */
@property (nonatomic , strong , readonly) RS_User *activeUser;

/**
 *  current users active cart
 */
@property (nonatomic , strong , readonly) RS_ShoppingCart *activeCart;

/**
 *  session state
 */
@property (nonatomic , assign , readonly) BOOL isActive;

/**
 *  shared singleton instance for manager
 *
 *  @return RS_UserSessionManager instance
 */
+(RS_UserSessionManager *) sharedManager;

/**
 *  starts session with valid user
 *
 *  @param user RS_User
 */
-(void) startSessionWithUser:(RS_User *) user;


/**
 *  Wrapper methode over adding product in users cart
 *
 *  @param product valid product with quatity
 *
 *  @return YES if product is added or quatity is increased otherwise NO
 */
-(BOOL) addProductToCart:(RS_Product *) product;

/**
 *  Wrapper methode for removing product in users cart
 *
 *  @param product product valid product with quatity
 *
 *  @return YES if product is removing removed or quatity is reduced otherwise NO
 */
-(BOOL) removeProductFromCart:(RS_Product *) product;

/**
 *  Syncs cart in local storage
 *
 *  @return YES if cart is synced successfully otherwise NO
 */
-(BOOL) saveCart;

@end
