//
//  RS_UserSessionManager.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_UserSessionManager.h"
#import "RS_User.h"
#import "RS_ShoppingCart.h"

NSString *const RS_UserSessionManagerCartDidUpdateNotification = @"RS_UserSessionManagerCartDidUpdateNotification";

static RS_UserSessionManager* sharedUserSessionManager = nil;

@implementation RS_UserSessionManager

+(RS_UserSessionManager *) sharedManager {
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedUserSessionManager = [RS_UserSessionManager new];
        
    });
    
    return sharedUserSessionManager;
}

-(instancetype) init {
    
    if (!sharedUserSessionManager) {
        sharedUserSessionManager = [super init];
    }
    
    return sharedUserSessionManager;
}

-(void) startSessionWithUser:(RS_User *) user {
    
    _isActive = NO;
    if (user) {
        _isActive = YES;
        _activeUser = user;
        RS_ShoppingCart *cart = [RS_ShoppingCart activeCartForUser:self.activeUser];
        [cart setUser:self.activeUser];
        if (cart) {
            _activeCart = cart;
        } else {
            
            _activeCart = [RS_ShoppingCart cartWithUser:self.activeUser andProducts:nil];
            [self.activeCart saveInDB];
        }
       
    }

}


-(BOOL) addProductToCart:(RS_Product *) product {
    
    BOOL status = [self.activeCart addProduct:product];
    
    if (status) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RS_UserSessionManagerCartDidUpdateNotification object:nil];
    }
    
    [self saveCart];
    
    return status;
    
}

-(BOOL) removeProductFromCart:(RS_Product *) product {
    
   BOOL status = [self.activeCart removeProduct:product];
    
    if (status) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RS_UserSessionManagerCartDidUpdateNotification object:nil];
    }
    
    return [self saveCart];
}

-(BOOL) saveCart {
    
    if (self.isActive) {
        return [self.activeCart saveInDB];
    }
    
    return NO;
}

@end
