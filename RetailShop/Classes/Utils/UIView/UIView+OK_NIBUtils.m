//
//  UIView+OK_NIBUtils.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "UIView+OK_NIBUtils.h"

@implementation UIView (OK_NIBUtils)

+(UINib *) ok_NibFromDefaultBundle:(NSString *)nibName {
    
    return [UINib nibWithNibName:nibName bundle:nil];
    
}


+(UINib *) ok_nibForClass:(Class)viewClass {
    NSString *classNameForNib = NSStringFromClass(viewClass);
    return [UINib nibWithNibName:classNameForNib bundle:nil];
}

@end
