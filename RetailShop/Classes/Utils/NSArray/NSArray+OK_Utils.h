//
//  NSArray+OK_Utils.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (OK_Utils)

- (NSArray*) ok_filteredArrayWithValue:(id)value forKeyPath:(NSString*)key;

@end
