//
//  NSArray+OK_Utils.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "NSArray+OK_Utils.h"

@implementation NSArray (OK_Utils)

- (NSArray*) ok_filteredArrayWithValue:(id)value forKeyPath:(NSString*)key
{
    NSMutableArray * objects = [NSMutableArray arrayWithCapacity:[self count]];
    
    for (id object in self) {
        if( [[object valueForKeyPath:key] isEqual:value] ) {
            [objects addObject:object];
        }
    }
    
    return [NSArray arrayWithArray:objects];
}

@end
