//
//  UIImage+OK_Utils.m
//  OK_Utils
//
//  Created by Omkar on 09/07/13.
//  Copyright (c) 2013 Oms. All rights reserved.
//

#import "UIImage+OK_Utils.h"

@implementation UIImage (OK_Utils)

+ (UIImage *)ok_imageWithColor:(UIColor *)color
{
    
    return [[self class] ok_imageWithColor:color andSize:CGSizeMake(1, 1)];
}

+ (UIImage *)ok_imageWithColor:(UIColor *)color andSize:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
