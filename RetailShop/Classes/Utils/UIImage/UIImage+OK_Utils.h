//
//  UIImage+OK_Utils.h
//  OK_Utils
//
//  Created by Omkar on 09/07/13.
//  Copyright (c) 2013 Oms. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (OK_Utils)
+ (UIImage *)ok_imageWithColor:(UIColor *)color;
+ (UIImage *)ok_imageWithColor:(UIColor *)color andSize:(CGSize)size;
@end
