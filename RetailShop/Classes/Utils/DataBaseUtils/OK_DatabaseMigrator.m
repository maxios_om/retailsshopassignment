//
// Copyright (c) 2012 Omkar Khedekar
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
//  OK_DatabaseMigrator.m
//
//  Created by Omkar Khedekar on 24/12/12.
//  Copyright (c) 2012 Omkar Khedekar. All rights reserved.
//

#import "OK_DatabaseMigrator.h"
#import "NSString+Common.h"

//if you need to force a migration # for whatever reason, uncomment this line.
//Just remember to put it back when you're done!
//#define FORCE_MIGRATION 0

@implementation OK_DatabaseMigrator

@synthesize filename = _filename, overwriteDatabase;

-(id)initWithDatabaseFile:(NSString *)filename {
	if (self = [super init]) {
		self.filename = filename;
		database = [[FMDatabase alloc] initWithPath:[self databasePath]];
#ifdef DEBUG
		[database setLogsErrors:YES];
#endif
		
	}
	
	return self;
}

-(NSString *)databasePath {
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:self.filename];
}

-(BOOL)moveDatabaseToUserDirectoryIfNeeded {
	NSString *databasePath = [self databasePath];
	
	NSFileManager *fileMgr = [NSFileManager defaultManager];
	if([fileMgr fileExistsAtPath:databasePath]) {
		if (overwriteDatabase) {
#ifdef DEBUG
			NSLog(@"Overwrite is set to YES, deleting old database file...");

#endif
			[fileMgr removeItemAtPath:databasePath error:nil];
		} else {
#ifdef DEBUG
            NSLog(@"Exists \n\n\n PATH %@",databasePath);
#endif
			return NO;
		}
	}
	
	NSArray *fileParts = [self.filename splitOnChar:'.'];
	if(fileParts == NULL || [fileParts count] < 2) {

		NSLog(@"Invalid filename passed to verifyWritableDatabase ==> %@", self.filename);
		[[NSException exceptionWithName:@"Invalid filename" reason:@"Expected a filename like foo.db" userInfo:nil] raise];
		exit(-1);
	}
	
	NSString *name = [fileParts objectAtIndex:0];
	NSString *extension = [fileParts objectAtIndex:1];
#ifdef DEBUG
	NSLog(@"Moving database from app package to user directory");
#endif
	NSString *dbPathFromAppPackage = [[NSBundle mainBundle] pathForResource:name ofType:extension];
	
	if(dbPathFromAppPackage == nil) {
		
		[[NSException exceptionWithName:@"Invalid resource path" reason:[NSString stringWithFormat:@"Couldn't find %@ in the bundle!", self.filename] userInfo:nil] raise];
		exit(1);
	}
	
	NSError *error;
	BOOL success = [fileMgr copyItemAtPath:dbPathFromAppPackage toPath:databasePath error:&error];
#ifdef DEBUG
	NSLog(@"done copying \n\n\n PATH %@",databasePath);
#endif
	if(!success) {	
		NSString *msg = [NSString stringWithFormat:@"Error moving database to user directory: %@", [error localizedDescription]];
#ifdef DEBUG
		NSLog(@"error: %@", msg);
#endif


	}
	
	return YES;
}

//gets the current version of the database
-(int)version {
	
#ifdef FORCE_MIGRATION
	[self setVersion:FORCE_MIGRATION];
#endif
	
	int version = [database intForQuery:@"PRAGMA user_version"];
	return version;
}

//sets the current version of the database
-(void)setVersion:(int)version {	
	[database executeUpdate:[NSString stringWithFormat:@"PRAGMA user_version=%d", version]];
}

//applies a migration file (migration-X.sql -- where X is the migration #)
//the first migration file would be 1 (since the db starts at version 0)
-(BOOL)applyMigration:(int)version {
	NSString *migrationFile = [NSString stringWithFormat:@"/migration-%d.sql", version];
#ifdef DEBUG
	NSLog(@"File: %@", migrationFile);
#endif
	NSString *fullPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:migrationFile];
#ifdef DEBUG
	NSLog(@"Path: %@", fullPath);
#endif
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
#ifdef DEBUG
		NSLog(@"WARNING: Couldn't find migration-%d.sql at %@", version, fullPath);

#endif

		return NO;
	}
	
	NSString *migrationSql = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:nil];
	
	@try {
		NSArray *statements = [migrationSql splitOnChar:';'];
		
		for(NSString *statement in statements) {
			NSString *cleanedStatement = [statement stringByStrippingWhitespace];
			
			if([cleanedStatement length] > 0) {
#ifdef DEBUG
				NSLog(@"==> Executing:    %@", cleanedStatement);
#endif
				[database executeUpdate:cleanedStatement];
				
				if ([database hadError]) {
					return NO;
				}
			}
		}
	}
	@catch (NSException *exception) {
#ifdef DEBUG
		NSLog(@"Error executing migration %d.  The error was: %@", version, exception);
		NSLog(@"Continuing anyway...");
#else
		[Flurry logEvent:[NSString stringWithFormat:@"Error executing migration %d.  The error was: %@", version, exception]];
#endif
	}
	
	return YES;
}

//applies all necessary migrations to bring this database up to the specified version
-(void)migrateToVersion:(NSInteger)version {
	[database open];
	NSInteger currentVersion = [self version];
	
	if(currentVersion == version) {
#ifdef DEBUG
		NSLog(@"No migration needed, already at version %d", (int)version);
#endif
		[database close];
		return;
	}
	
	
	BOOL success = NO;
	for(NSInteger m = currentVersion + 1; m <= version; m++) {
#ifdef DEBUG
		NSLog(@"Running migration %d", (int)m);
#endif
		success = [self applyMigration:(int)m];
		
		if (!success) {
#ifdef DEBUG
			NSLog(@"Error executing migration %d", (int)m);
#endif
			break;
		}
		
		//update to the latest successful migration
		[self setVersion:(int)m];
	}
	
	NSLog(@"Done with migrations....");
	if(!success) {
#ifdef DEBUG

		NSLog(@"There were errors during the migration.  The current version is %d", [self version]);
#endif
	} else {
#ifdef DEBUG
		NSLog(@"Successfully migrated to version %d", (int)version);
#endif
	}
}

-(void)dealloc {
    
	self.filename = nil;
}

@end
