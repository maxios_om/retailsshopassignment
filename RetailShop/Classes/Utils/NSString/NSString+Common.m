//
// Copyright (c) 2012 Omkar Khedekar
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
//  NSString+Common.m
//
//  Created by Omkar Khedekar on 24/12/12.
//  Copyright (c) 2012 Omkar Khedekar. All rights reserved.
//

#import "NSString+Common.h"

@implementation NSString (Common)

-(BOOL)isBlank {
	if([[self stringByStrippingWhitespace] length] == 0)
		return YES;
	return NO;
}

-(BOOL)contains:(NSString *)string {
	NSRange range = [self rangeOfString:string];
	return (range.location != NSNotFound);
}

-(NSString *)stringByStrippingWhitespace {
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSArray *)splitOnChar:(char)ch {
	NSMutableArray *results = [[NSMutableArray alloc] init];
	int start = 0;
	for(int i=0; i<[self length]; i++) {
		
		BOOL isAtSplitChar = [self characterAtIndex:i] == ch;
		BOOL isAtEnd = i == [self length] - 1;
				
		if(isAtSplitChar || isAtEnd) {
			//take the substring & add it to the array
			NSRange range;
			range.location = start;
			range.length = i - start + 1;
			
			if(isAtSplitChar)
				range.length -= 1;
			
			[results addObject:[self substringWithRange:range]];
			start = i + 1;
		}
		
		//handle the case where the last character was the split char.  we need an empty trailing element in the array.
		if(isAtEnd && isAtSplitChar)
			[results addObject:@""];
	}
	
	return results;
}
	
-(NSString *)substringFrom:(NSInteger)from to:(NSInteger)to {
	NSString *rightPart = [self substringFromIndex:from];
	return [rightPart substringToIndex:to-from];
}	


/**
 * This method will return NSNotFound as location if the str is not found. 
 */
-(NSInteger) indexOfString: (NSString *) str {
	NSRange range = [self rangeOfString:str];
	return range.location;
}


/**
 * This method will return NSNotFound as location if the str is not found. 
 */
-(NSInteger) lastIndexOfString:(NSString *) str {
	NSRange range = [self rangeOfString:str options:NSBackwardsSearch];
	return range.location;
}

-(BOOL) equalsIgnoreCase:(NSString *) str {
	if( [self caseInsensitiveCompare:str] == NSOrderedSame ) {
		return YES;
	} else {
		return NO;
	}

}

@end

NSString * EmptyIfNull(NSString * inputString) {
	if (inputString == nil) {
		return @"";
	}
	
	return inputString;
}
