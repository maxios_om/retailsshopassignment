//
//  AppDelegate.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AppDelegate.h"
#import "AbstractRepository.h"
#import "OK_DatabaseMigrator.h"
#import "RS_UserSessionManager.h"
#import "RS_User.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self initializeDB];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName:[UIColor colorWithRed:0.188 green:0.514 blue:0.784 alpha:1.000],
                                                           NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:14]
                                                           }];
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:0.188 green:0.514 blue:0.784 alpha:1.000]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[RS_UserSessionManager sharedManager] saveCart];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
#ifdef DUMMY_USER_ENABLED
    
    RS_User * dummy = [RS_User dummyUser];
    [[RS_UserSessionManager sharedManager] startSessionWithUser:dummy];
    
#endif
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    [[RS_UserSessionManager sharedManager] saveCart];
    
}

#pragma mark - setup methodes

-(void)initializeDB {
    
    OK_DatabaseMigrator *migrator = [[OK_DatabaseMigrator alloc] initWithDatabaseFile:[AbstractRepository databaseFilename]];
    [migrator moveDatabaseToUserDirectoryIfNeeded];
    
    
}

@end
