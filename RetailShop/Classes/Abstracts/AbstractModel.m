//
//  AbstractModel.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"
#import <objc/runtime.h>

@implementation AbstractModel

-(instancetype) init {
    
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(instancetype) initWithJSONDict:(NSDictionary *)json  {
    
    self = [super init];
    if (self) {
        // Do some thing
    }
    return self;
    
}


+(instancetype) instanceFromJSON:(NSDictionary *)json {
    
    return [[[self class] alloc] initWithJSONDict :json];
}


- (NSDictionary *)dictionaryRepresentation {
    
    unsigned int count = 0;
    
    
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        if (value)
            [dictionary setObject:value forKey:key];
    }
    free(properties);
    return dictionary;
}
@end
