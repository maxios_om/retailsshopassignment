//
//  AbstractRepository.m
//  RetailShop
//
//  Created by omkar.khedekar on 4/22/15.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"

@implementation AbstractRepository

+(NSString *)databaseFilename {

//#warning "Must provide db file name"

//    return @"<YOUR DATA BASE FILE NAME>";

    return @"retail_shop_db.sqlite";
}


+(NSString *) databasePath {
    NSString *filename = [AbstractRepository databaseFilename];

    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * dbfilePath = [documentsDirectory stringByAppendingPathComponent:filename];

    return dbfilePath;
}

-(id)init {
    self = [super init];
    
    if (self) {
        NSString *dbPath = [AbstractRepository databasePath];
        db = [FMDatabase databaseWithPath:dbPath];
#ifdef DEBUG
        [db setTraceExecution:YES];
        [db setLogsErrors:YES];
#endif
        [db open];
    }

    return self;
}



-(NSInteger) lastInsertRowId {
    NSString * query = @"SELECT last_insert_rowid()";
    NSInteger rowId = [db intForQuery:query];
    return rowId;
}


@end
