//
//  AbstractModel.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 
 @abstract abstraction Layer as protocall
 
 @name AbstractModel
 
*/

@protocol AbstractModel <NSObject>

+(instancetype) instanceFromJSON:(NSDictionary *)json;
-(instancetype) initWithJSONDict:(NSDictionary *)json;
-(NSDictionary *)dictionaryRepresentation;

@end

/*!
 
 @abstract data base Convenience methodes
 
 @name AbstractModelDBProtocall
 
 */
@protocol AbstractModelDBProtocall <NSObject>

@optional
/**
 *  convniance method that sub classes can write there own Data base update and add logic into
 *
 *  @return <#return value description#>
 */

-(BOOL)saveInDB;

@end

/*!
 
 @type Abstract Base Class
 
 @name AbstractModel
 
*/
@interface AbstractModel : NSObject <AbstractModel , AbstractModelDBProtocall>

/**
 *  convniance initiliser method all subclasses should use same
 *
 *  @param json valid JSON dict
 *
 *  @return new instance
 */
+(instancetype) instanceFromJSON:(NSDictionary *)json ;

/**
 *  initiliser method all subclasses should use same
 *
 *  @param json valid JSON dict
 *
 *  @return new instance
 */
-(instancetype) initWithJSONDict:(NSDictionary *)json ;

/**
 *  Dictioney representation of model propery names as key and propery as value
 *
 *  @return NSDictionary instance
 */
-(NSDictionary *)dictionaryRepresentation;

@end
