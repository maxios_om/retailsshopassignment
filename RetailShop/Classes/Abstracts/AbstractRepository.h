//
//  AbstractRepository.h
//  RetailShop
//
//  Created by omkar.khedekar on 4/22/15.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@protocol AbstractRepositoryProtocol

@optional
/*
 Provides status of record based on record ID
*/

-(BOOL)isRecordExist:(NSString *)primeryKey;


@end

/**
 *  Pure Data access class for models serves as abstration layes for data base intraction
 */
@interface AbstractRepository : NSObject <AbstractRepositoryProtocol> {
    //Protected instance so only subs can use this
    FMDatabase *db;
}




/**
 *  Data base file namme
 *
 *  @return name of SQLite DB file 
 */
+(NSString *)databaseFilename;


/**
 *  returns last inserted row id helpfull for adding all new objects which dont have any ID assigned
 *
 *  @return last inserted row id
 */
-(NSInteger) lastInsertRowId;



@end
