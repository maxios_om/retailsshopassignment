//
//  ProductTableViewCell.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Cell types
 */
typedef NS_ENUM(NSInteger, RS_ProductTableViewCellType) {
    /**
     *  normal cell used for showing product
     */
    RS_ProductTableViewCellTypeProductCell = 0,
    /**
     *  cart cell populates with delete button to remove product
     */
    RS_ProductTableViewCellTypeCartItemCell
};

@class RS_Product;

/**
 *  delete product handler block works only when cell type is @RS_ProductTableViewCellTypeCartItemCell
 *
 *  @param product product to be removed
 */
typedef void(^ProductTableViewCellRemoveProductCallBack)(RS_Product *product);

/**
 *  TableViewCell for Product
 */
@interface RS_ProductTableViewCell : UITableViewCell

/**
 *  Populates cell with product and type
 */
-(void) populateWithProduct:(RS_Product *) product andCellType:(RS_ProductTableViewCellType) productTableViewCellType;


/**
 *  sets delete product call back block 
 */
-(void) addRemoveProductCallBack:(ProductTableViewCellRemoveProductCallBack) callback;

@end
