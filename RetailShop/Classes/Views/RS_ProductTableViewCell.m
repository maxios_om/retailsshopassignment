//
//  ProductTableViewCell.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductTableViewCell.h"
#import "RS_Product.h"
#import "RS_ProductDetails.h"
#import "RS_Category.h"
#import "RS_ProductType.h"



@interface RS_ProductTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCategoryLebel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic , strong) RS_Product *product;
@property (nonatomic , copy) ProductTableViewCellRemoveProductCallBack removeProductCallBack;

@end

@implementation RS_ProductTableViewCell

-(void) configureQuantity {
    
    NSMutableAttributedString *quatityString = [[NSMutableAttributedString alloc] init];
    
    NSAttributedString *quatityTitle = [[NSAttributedString alloc] initWithString:@"Quantity: " attributes:@{
                                                                                                             NSForegroundColorAttributeName:[UIColor colorWithWhite:0.498 alpha:1.000],
                                                                                                             NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12]
                                                                                                             }];
    [quatityString appendAttributedString:quatityTitle];
    
    
    NSAttributedString *quatityValue = [[NSAttributedString alloc] initWithString:[@(self.product.quantity) stringValue] attributes:@{
                                                                                                                                NSForegroundColorAttributeName:[UIColor colorWithWhite:0.800 alpha:1.000],
                                                                                                                                NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12]
                                                                                                                                }];
    
    
    [quatityString appendAttributedString:quatityValue];
    [self.productCategoryLebel setAttributedText:quatityString];
}

-(void) populateWithProduct:(RS_Product *) product andCellType:(RS_ProductTableViewCellType)productTableViewCellType {
    self.product = product;
    UIImage *productImage = [UIImage imageNamed:product.details.mainImageName];
    
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    
    [self.containerView.layer setBorderWidth:1];
    [self.containerView.layer setBorderColor:[[UIColor colorWithRed:0.188 green:0.514 blue:0.784 alpha:0.900] CGColor]];
    [self.containerView setBackgroundColor:[UIColor whiteColor]];
    
    [self.productImageView setImage:productImage];
    [self.productTitleLabel setText:product.productName];
    
    if (productTableViewCellType == RS_ProductTableViewCellTypeCartItemCell) {
        [self.deleteButton setHidden:NO];
        [self.productPriceLabel setText:[product localisedPriceByQuantity]];
        [self configureQuantity];
        
    } else {
        [self.productCategoryLebel setText:product.type.category.categoryName];
        [self.productPriceLabel setText:[product localisedPrice]];
        [self.deleteButton setHidden:YES];
    }
    
}

-(void) addRemoveProductCallBack:(ProductTableViewCellRemoveProductCallBack) callback {
    
    self.removeProductCallBack = callback;
}

#pragma mark - Actions
- (IBAction)deleteButtonTapped:(id)sender {
    
    if (_removeProductCallBack) {
        self.removeProductCallBack(self.product);
    }
}

@end
