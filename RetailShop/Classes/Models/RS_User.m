//
//  RS_User.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_User.h"
#import "RS_UserRepository.h"

@implementation RS_User

+(RS_User *) dummyUser {
    
    RS_User *dummy = [RS_User new];
    
    [dummy setUserID:-1];
    [dummy setUserName:@"Dummy User"];
    [dummy setUserEmail:@"dummy@retail.shop"];
    BOOL saved = [dummy saveInDB];
    if (saved) {
        return dummy;
    }
    return nil;
}


+(RS_User *) userWithID:(NSInteger) userID {

    return [[RS_UserRepository new] userWithID:userID];
    
}

+(RS_User *) userWithEmail:(NSString *) emailID {
    
    return [[RS_UserRepository new] userWithEmail:emailID];
}


-(BOOL) saveInDB {
    
    RS_UserRepository *repo = [RS_UserRepository new];
    
    BOOL isExistingUser = [repo isRecordExist:self.userEmail];
    
    if (isExistingUser) {
        return [repo updateUser:self];
    } else {
        return [repo addUser:self];
    }
    
    
}

@end
