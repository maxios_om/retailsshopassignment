//
//  RS_Category.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"

/**
 *  RS_Category
 */
@interface RS_Category : AbstractModel

/**
 *  categoryID
 */
@property (nonatomic , assign) NSInteger categoryID;

/**
 *  categoryName
 */
@property (nonatomic , copy) NSString *categoryName;

@end
