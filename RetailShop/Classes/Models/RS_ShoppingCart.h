//
//  RS_ShoppingCart.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"
// future
/*
typedef NS_ENUM(NSInteger , RS_ShoppingCartStatus) {
    
    RS_ShoppingCartStatusEmpty = -1,
    RS_ShoppingCartStatusActive = 0,
    RS_ShoppingCartStatusProcessed = 1

};
*/

@class RS_User;
@class RS_Product;
/**
 *  RS_ShoppingCart
 */
@interface RS_ShoppingCart : AbstractModel

/**
 *  cartID
 */
@property (nonatomic , assign) NSInteger cartID;

/**
 *  cart owener
 */
@property (nonatomic , strong) RS_User *user;

/**
 *  cart status processed or not
 */
@property (nonatomic , assign) BOOL cartStatus;

/**
 *  has any products?
 */
@property (nonatomic , assign , readonly) BOOL isEmpty;

/**
 *  Careates new cart with owener and product
 *
 *  @param user     owener
 *  @param products products if any
 *
 *  @return cart
 */
+(instancetype) cartWithUser:(RS_User *)user andProducts:(NSMutableArray<RS_Product *> *)products;

/**
 *  returns active cart for user if exist from DB
 *
 *  @param user owener
 *
 *  @return cart
 */
+(instancetype) activeCartForUser:(RS_User *)user;

/**
 *  adds product in cart
 *
 *  @param product
 *
 *  @return YES IF added NO otherwise
 */
-(BOOL) addProduct:(RS_Product *) product;

/**
 *  removes product in cart
 *
 *  @param product
 *
 *  @return YES IF removed NO otherwise
 */
-(BOOL) removeProduct:(RS_Product *) product;

/**
 *  Returns all products with quantity available in cart
 *
 *  @return array of products
 */
-(NSMutableArray<RS_Product *> *) productsInCart;

/**
 *  Total cart price as deicmal number
 */
-(NSDecimalNumber *) totalCartPrice;

/**
 *  returns calculated Total cart price string per devices local
 *
 */
-(NSString *) localisedTotalCartPrice;


@end
