//
//  RS_ShoppingCart.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCart.h"
#import "RS_User.h"
#import "RS_Product.h"
#import "RS_ShoppingCartRepository.h"
#import "RS_ShoppingCartItem.h"

static NSNumberFormatter *numberFormatter = nil;

@interface RS_ShoppingCart ()
@property (nonatomic , strong) NSMutableArray<RS_Product*> *products;
@end

@implementation RS_ShoppingCart


-(NSNumberFormatter *) currencyFormatter {
    
    if (!numberFormatter) {
        numberFormatter = [[NSNumberFormatter alloc] init];
#if (TARGET_IPHONE_SIMULATOR)
        [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_IN"]];
        [numberFormatter setCurrencySymbol:@"Rs."];
#else
        [numberFormatter setLocale:[NSLocale currentLocale]];
#endif
        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    }
    
    return numberFormatter;
}


+(instancetype) cartWithUser:(RS_User *)user andProducts:(NSMutableArray<RS_Product *> *)products {
   
    RS_ShoppingCart *cart = [RS_ShoppingCart new];
    
    cart.user = user;
    
    cart.products = [products mutableCopy];
    
    return cart;
}


+(instancetype) activeCartForUser:(RS_User *)user {
    
    RS_ShoppingCartRepository *cartRepo = [RS_ShoppingCartRepository new];
    
    RS_ShoppingCart *cart = [cartRepo activeShoppingCartForUser:user.userID];
    
    NSArray *allCartItems = [RS_ShoppingCartItem allProductsForCart:cart.cartID];

    NSMutableArray <RS_Product*> *allProducts = [NSMutableArray array];
    for (RS_ShoppingCartItem *item in allCartItems) {
        
        [allProducts addObject:item.product];
        
    }
    
    [cart setProducts:[NSMutableArray arrayWithArray:allProducts]];
    
    return cart;
}


-(NSArray <RS_ShoppingCartItem*> *) cartItems {
    
    NSMutableArray *allItems = [NSMutableArray array];
    
    for (RS_Product *product in self.products) {
        
        RS_ShoppingCartItem *item = [RS_ShoppingCartItem shoppingCartItemForCart:self.cartID withProduct:product];
        [allItems addObject:item];
    }
    
    return allItems;
}

-(NSMutableArray<RS_Product*> *) products {
    
    if (!_products) {
        _products = [NSMutableArray array];
    }
    
    return _products;
}

-(NSMutableArray<RS_Product *> *) productsInCart {
    
    return self.products;
    
}

-(BOOL) isEmpty {
    
    if (_products && [_products count] > 0) {
        return NO;
    }
    
    return YES;
}

-(BOOL) addProduct:(RS_Product *) product {
    
    if ([RS_Product isValidProduct:product]) {
        
        if ([self.products containsObject:product]) {
            
            RS_Product *existingProduct = [self.products objectAtIndex:[self.products indexOfObject:product]];
            
            existingProduct.quantity = existingProduct.quantity + 1;
            
        } else {
            
            product.quantity = 1;
            [self.products addObject:product];
        }
        
        
        return YES;
    }
    
    return NO;
}


-(BOOL) removeProduct:(RS_Product *) product {
    
    if ([RS_Product isValidProduct:product] && [self.products containsObject:product]) {
        
        RS_Product *existingProduct = [self.products objectAtIndex:[self.products indexOfObject:product]];
        RS_ShoppingCartItem *itemToUpdate = [RS_ShoppingCartItem shoppingCartItemForCart:self.cartID withProduct:existingProduct];
        
        if (existingProduct.quantity > 1) {
            
            existingProduct.quantity = existingProduct.quantity - 1;
            
            [itemToUpdate saveInDB];
            
        } else {
        
            [self.products removeObject:product];
            [itemToUpdate deleteItem];
        }
        
        return YES;
    }
    
    return NO;
}


-(NSDecimalNumber *)totalCartPrice {
    
    NSDecimalNumber *price = [NSDecimalNumber zero];
    
    for (RS_Product *product in self.products) {
        
        NSDecimalNumber *quantitymultiplyer = [[NSDecimalNumber alloc] initWithInteger:product.quantity];
        
        NSDecimalNumber *aggrPrice = [product.productPrice decimalNumberByMultiplyingBy:quantitymultiplyer];
        
        price = [price decimalNumberByAdding:aggrPrice];
    }
    
    return price;
}

-(NSString *) localisedTotalCartPrice {
    
    NSString *formattedPrice = [[self currencyFormatter]  stringFromNumber:[self totalCartPrice]];
    
    return formattedPrice;
}

-(BOOL) saveInDB {
    
    
    BOOL allItemsSaveStatus = [RS_ShoppingCartItem saveCartItems:[self cartItems] forCartID:self.cartID];
    
    if (allItemsSaveStatus) {
        RS_ShoppingCartRepository *repo = [RS_ShoppingCartRepository new];
        
        BOOL exists = [repo isRecordExist:[@(self.user.userID) stringValue]];
        
        
        if (exists) {
            
            return [repo updateShoppingCart:self];
            
        } else {
            
            return [repo addShoppingCart:self];
        }
    }

    return NO;
}

@end
