//
//  RS_User.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"

/**
 *  User class
 */
@interface RS_User : AbstractModel

/**
 *  userID
 */
@property (nonatomic , assign) NSInteger userID;

/**
 *  userName
 */
@property (nonatomic , copy) NSString* userName;

/**
 *  userEmail
 */
@property (nonatomic , copy) NSString *userEmail;


/**
 *  Data base conveniance methode to fetch user by id
 *
 *  @param userID user id
 *
 *  @return RS_User if exist otherwise nil
 */
+(RS_User *) userWithID:(NSInteger) userID;

/**
 *  Data base conveniance methode to fetch user by email
 *
 *  @param emailID emailID
 *
 *  @return RS_User if exist otherwise nil
 */
+(RS_User *) userWithEmail:(NSString *) emailID;

/**
 *  dummy user for testing
 *
 */
#ifdef DUMMY_USER_ENABLED
+(RS_User *) dummyUser;
#endif

@end
