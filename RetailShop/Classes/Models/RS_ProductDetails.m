//
//  RS_ProductDetails.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductDetails.h"
#import "RS_ProductDetailsRepository.h"

@implementation RS_ProductDetails

+(RS_ProductDetails *) productDetailsForProductID:(NSInteger) productID {
    
    return [[RS_ProductDetailsRepository new] productDetailsForProductID:productID];
}

@end
