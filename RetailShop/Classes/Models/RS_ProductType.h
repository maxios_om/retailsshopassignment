//
//  RS_ProductType.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"

@class RS_Category;

/**
 *  model to hold relatation between categoty and type as sub-category
 */
@interface RS_ProductType : AbstractModel

/**
 *  productTypeID
 */
@property (nonatomic , assign) NSInteger productTypeID;

/**
 *  category that type belongs to
 */
@property (nonatomic , strong) RS_Category *category;


@end
