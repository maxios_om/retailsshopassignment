//
//  RS_ShoppingCartItem.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCartItem.h"
#import "RS_ShoppingCartItemRepository.h"

@implementation RS_ShoppingCartItem

+(RS_ShoppingCartItem *) shoppingCartItemForCart:(NSInteger) cartID withProduct:(RS_Product *) product {
    
    RS_ShoppingCartItem * item = [RS_ShoppingCartItem new];
    
    item.cartID = cartID;
    item.product = product;
    
    return item;
}

+(NSArray <RS_ShoppingCartItem *> *) allProductsForCart:(NSInteger) cartID {
 
    return [[RS_ShoppingCartItemRepository new] allProductsForCart:cartID];
}


+(BOOL) saveCartItems:(NSArray <RS_ShoppingCartItem*> *) items forCartID:(NSInteger) cartID {
    
    BOOL status = YES;
    
    for (RS_ShoppingCartItem *item in items) {
        
        item.cartID = cartID;
        status = [item saveInDB];
        if (!status) {
            break;
        }
    }
    return status;
}

-(BOOL) saveInDB {
    
    RS_ShoppingCartItemRepository *repo = [RS_ShoppingCartItemRepository new];
    
    BOOL isExist = [repo isExistingItem:self];
    
    if (isExist) {
        return [repo updateShoppingCartItem:self];
    } else {
        return [repo addShoppingCartItem:self];
    }
}

-(BOOL) deleteItem {
    
    return [[RS_ShoppingCartItemRepository new] deleteItem:self];
}

@end
