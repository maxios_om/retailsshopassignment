//
//  RS_Product.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"

@class RS_ProductType;
@class RS_ProductDetails;

@interface RS_Product : AbstractModel

@property (nonatomic , assign) NSInteger productID;
@property (nonatomic , copy) NSString* productName;
@property (nonatomic , copy) NSDecimalNumber *productPrice;
@property (nonatomic , strong) RS_ProductType *type;
@property (nonatomic , strong) RS_ProductDetails *details;
@property (nonatomic , assign) NSInteger quantity;

+(NSArray <RS_Product *> *) allProducts;

+(NSArray <RS_Product *> *) allProductsForCategoryID:(NSInteger) categoryID;

+(NSArray <RS_Product *> *) allProductsForProductTypeID:(NSInteger) productTypeID;

+(RS_Product *) productForProductID:(NSInteger) productID;

-(NSString *) localisedPrice;

-(NSString *) localisedPriceByQuantity;

-(BOOL) isEqual:(id)object;

@end

@interface RS_Product (ProductValidation)

+(BOOL) isValidProduct:(id)product;

@end