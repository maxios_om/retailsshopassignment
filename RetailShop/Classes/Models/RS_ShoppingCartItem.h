//
//  RS_ShoppingCartItem.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"

@class RS_Product;
/**
 *  pure data holder model for DB
 */
@interface RS_ShoppingCartItem : AbstractModel

/**
 * itemId
 */
@property (nonatomic , assign) NSInteger itemId;
/**
 *  cartID
 */
@property (nonatomic , assign) NSInteger cartID;

/**
 *  product
 */
@property (nonatomic , strong) RS_Product *product;

/**
 *  Conveniance method to create RS_ShoppingCartItem for cart with product
 */
+(RS_ShoppingCartItem *) shoppingCartItemForCart:(NSInteger) cartID withProduct:(RS_Product *) product;

/**
 *  returns all products for cart from DB
 *
 */
+(NSArray <RS_ShoppingCartItem *> *) allProductsForCart:(NSInteger) cartID;

/**
 *  saves cart items back to db for cart
 *
 */
+(BOOL) saveCartItems:(NSArray <RS_ShoppingCartItem*> *) items forCartID:(NSInteger) cartID;

/**
 *  delets existing cart item
 *
 *  @return YES if delete is successful no otherwise
 */
-(BOOL) deleteItem;

@end
