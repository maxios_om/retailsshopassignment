//
//  RS_ProductDetails.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractModel.h"

/**
 *  RS_ProductDetails
 */
@interface RS_ProductDetails : AbstractModel

/**
 *  productDetailsID
 */
@property (nonatomic , assign) NSInteger productDetailsID;

/**
 *  productID
 */
@property (nonatomic , assign) NSInteger productID;
/**
 *  productImages array
 */
@property (nonatomic , copy) NSArray *productImages;

/**
 *  mainImageName
 */
@property (nonatomic , copy) NSString *mainImageName;

/**
 *  Product info details 
 */
@property (nonatomic , copy) NSString *detailsText;

/**
 *  Data base conveniance methode to fetch product details for provided product id
 *
 *  @param productID productID
 *
 *  @return YES if details available nil otherwise
 */
+(RS_ProductDetails *) productDetailsForProductID:(NSInteger) productID;

@end
