//
//  RS_Product.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_Product.h"
#import "RS_ProductRepository.h"
#import "RS_ProductDetails.h"

static NSNumberFormatter *numberFormatter = nil;



@implementation RS_Product


-(NSNumberFormatter *) currencyFormatter {
    
    if (!numberFormatter) {
        numberFormatter = [[NSNumberFormatter alloc] init];
#if (TARGET_IPHONE_SIMULATOR)
        [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_IN"]];
        [numberFormatter setCurrencySymbol:@"Rs."];
#else
        [numberFormatter setLocale:[NSLocale currentLocale]];
#endif
        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    }
    
    return numberFormatter;
}

-(RS_ProductDetails *) details {
    
    if (!_details) {
        _details = [RS_ProductDetails productDetailsForProductID:self.productID];
        
    }
    
    return _details;
}

-(NSString *) localisedPrice {
    
    NSString *formattedPrice = [[self currencyFormatter]  stringFromNumber:self.productPrice];
    
    return formattedPrice;
    
}

-(NSString *) localisedPriceByQuantity {
    
    NSDecimalNumber *finalPrice = self.productPrice;
    if (self.quantity > 1) {
        NSDecimalNumber *quantitymultiplyer = [[NSDecimalNumber alloc] initWithInteger:self.quantity];
        
         finalPrice = [self.productPrice decimalNumberByMultiplyingBy:quantitymultiplyer];
    }
    
    NSString *formattedPrice = [[self currencyFormatter]  stringFromNumber:finalPrice];
    
    return formattedPrice;
    
}


+(NSArray <RS_Product *> *) allProducts {
    
    NSArray *allProducts = [[RS_ProductRepository new] allProducts];
    
    return allProducts;
}


+(NSArray <RS_Product *> *) allProductsForCategoryID:(NSInteger) categoryID {
    
    NSArray *allProducts = [[RS_ProductRepository new] allProductsForCategoryID:categoryID];
    
    return allProducts;
}

+(NSArray <RS_Product *> *) allProductsForProductTypeID:(NSInteger) productTypeID {
 
    NSArray *allProducts = [[RS_ProductRepository new] allProductsForProductTypeID:productTypeID];
    
    return allProducts;
    
}


+(RS_Product *) productForProductID:(NSInteger) productID {

    return [[RS_ProductRepository new] productForProductID:productID];

}


-(BOOL) isEqual:(id)object {
    
    if (![RS_Product isValidProduct:object]) {
        return NO;
    }
    
    RS_Product *castedObject = (RS_Product *)object;
    if (castedObject.productID == self.productID) {
        
        return YES;
    }
    
    return NO;
}

@end


@implementation RS_Product (ProductValidation)

+(BOOL) isValidProduct:(id)product {
    
    if (product && [product isKindOfClass:[RS_Product class]]) {
        return YES;
    }
    
    return NO;
}

@end