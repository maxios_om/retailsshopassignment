//
//  RS_ProductDetailsRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductDetailsRepository.h"
#import "RS_ProductDetails+DBUtility.h"

@implementation RS_ProductDetailsRepository

-(BOOL) isRecordExist:(NSString *)primeryKey {

    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_product_details WHERE product_id = ?", primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}



-(RS_ProductDetails *) productDetailsForProductID:(NSInteger) productID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "product_details_id, product_id, product_images, main_image_name, details_text"
                       " FROM "
                       "rs_product_details"
                       " WHERE "
                       "product_id = ?", @(productID)];
    
    
    RS_ProductDetails *productDetails = nil;
    while ([rs next]) {
        
        productDetails  = [RS_ProductDetails productDetailsFromResultSet:rs];
        
    }
    
    [rs close];
    
    return productDetails;
}


@end
