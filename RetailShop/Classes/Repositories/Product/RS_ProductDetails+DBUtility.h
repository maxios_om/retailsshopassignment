//
//  RS_ProductDetails+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductDetails.h"
@class FMResultSet;

@interface RS_ProductDetails (DBUtility)

+(RS_ProductDetails *) productDetailsFromResultSet:(FMResultSet *)resultSet;

@end
