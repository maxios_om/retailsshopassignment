//
//  RS_ProductDetails+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductDetails+DBUtility.h"
#import "AbstractRepository.h"

@implementation RS_ProductDetails (DBUtility)

+(RS_ProductDetails *) productDetailsFromResultSet:(FMResultSet *)resultSet {
    
    RS_ProductDetails *productDetails = [RS_ProductDetails new];
    
    productDetails.productDetailsID = [resultSet intForColumn:@"product_details_id"];
    productDetails.productDetailsID = [resultSet intForColumn:@"product_id"];
    productDetails.detailsText = [resultSet stringForColumn:@"details_text"];
    NSString *commaSeperatedImageNames = [resultSet stringForColumn:@"product_images"];
    NSArray *imageNamess = nil;
    if (commaSeperatedImageNames) {
    
        imageNamess = [commaSeperatedImageNames componentsSeparatedByString:@","];
    }
    
    productDetails.productImages = imageNamess;
    productDetails.mainImageName = [resultSet stringForColumn:@"main_image_name"];
    
    return productDetails;
}


@end
