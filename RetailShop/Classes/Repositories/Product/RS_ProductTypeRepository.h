//
//  RS_ProductTypeRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"

@class RS_ProductType;

@interface RS_ProductTypeRepository : AbstractRepository

-(NSArray <RS_ProductType *> *) allProductTypes;
-(NSArray <RS_ProductType *> *) allProductTypesForCategoryID:(NSInteger) categoryID;
-(RS_ProductType *) productTypeForProductTypeID:(NSInteger) productTypeID;

@end
