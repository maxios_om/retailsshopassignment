//
//  RS_ProductType+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductType.h"

@class FMResultSet;

@interface RS_ProductType (DBUtility)

+(RS_ProductType *) productTypeFromResultSet:(FMResultSet *) resultSet;

@end
