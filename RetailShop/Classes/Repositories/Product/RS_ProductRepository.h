//
//  RS_ProductRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"
@class RS_Product;


@interface RS_ProductRepository : AbstractRepository

-(NSArray <RS_Product *> *) allProducts;

-(NSArray <RS_Product *> *) allProductsForCategoryID:(NSInteger) categoryID;

-(NSArray <RS_Product *> *) allProductsForProductTypeID:(NSInteger) productTypeID;

-(RS_Product *) productForProductID:(NSInteger) productID;

@end

