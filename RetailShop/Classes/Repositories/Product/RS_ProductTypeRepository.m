//
//  RS_ProductTypeRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductTypeRepository.h"
#import "RS_ProductType.h"
#import "RS_ProductType+DBUtility.h"

@implementation RS_ProductTypeRepository

-(BOOL) isRecordExist:(NSString *)primeryKey {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_product_type WHERE product_type_id = ?", primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}

-(NSArray <RS_ProductType *> *) allProductTypes {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rspc.product_category_id, rspc.product_category_name,"
                       "rspt.product_type_id, rspt.product_category_id"
                       " FROM "
                       "rs_product_category as rspc,"
                       "rs_product_type as rspt"
                       " WHERE "
                       "rspt.product_category_id = rspc.product_category_id"];
    
    NSMutableArray *allProductTypes = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_ProductType *productType  = [RS_ProductType productTypeFromResultSet:rs];
        
        [allProductTypes addObject:productType];
        
    }
    
    [rs close];
    
    return allProductTypes;
}


-(NSArray <RS_ProductType *> *) allProductTypesForCategoryID:(NSInteger) categoryID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rspc.product_category_id, rspc.product_category_name,"
                       "rspt.product_type_id, rspt.product_category_id"
                       " FROM "
                       "rs_product_category as rspc,"
                       "rs_product_type as rspt"
                       " WHERE "
                       "rspt.product_category_id = rspc.product_category_id"
                       " AND "
                       "rspc.product_category_id = ?", @(categoryID)];
    
    NSMutableArray *allProductTypes = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_ProductType *productType  = [RS_ProductType productTypeFromResultSet:rs];
        
        [allProductTypes addObject:productType];
        
    }
    
    [rs close];
    
    return allProductTypes;
}


-(RS_ProductType *) productTypeForProductTypeID:(NSInteger) productTypeID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rspc.product_category_id, rspc.product_category_name,"
                       "rspt.product_type_id, rspt.product_category_id"
                       " FROM "
                       "rs_product_category as rspc,"
                       "rs_product_type as rspt"
                       " WHERE "
                       "rspt.product_category_id = rspc.product_category_id"
                       " AND "
                       "rspt.product_type_id = ?", @(productTypeID)];
    
    
    RS_ProductType *productType = nil;
    while ([rs next]) {
        
        productType  = [RS_ProductType productTypeFromResultSet:rs];
        
    }
    
    [rs close];
    
    return productType;
}

@end
