//
//  RS_ProductRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductRepository.h"
#import "RS_Product.h"
#import "RS_Product+DBUtility.h"
#import "RS_ProductTypeRepository.h"
#import "RS_CategoryRepository.h"

@implementation RS_ProductRepository


-(BOOL) isRecordExist:(NSString *)primeryKey {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_product WHERE product_id = ?", primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}

-(NSArray <RS_Product *> *) allProducts {
   
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rsp.product_id, rsp.product_type_id, rsp.product_name, rsp.product_price,"
                       "rspc.product_category_id, rspc.product_category_name,"
                       "rspt.product_type_id, rspt.product_category_id"
                       " FROM "
                       "rs_product as rsp,"
                       "rs_product_category as rspc,"
                       "rs_product_type as rspt"
                       " WHERE "
                       "rspt.product_category_id = rspc.product_category_id"
                       " AND "
                       "rsp.product_type_id = rspt.product_type_id"];
    
    NSMutableArray *allProducts = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_Product *product  = [RS_Product productFromResultSet:rs];
        
        [allProducts addObject:product];
        
    }
    
    [rs close];
    
    return allProducts;
}


-(NSArray <RS_Product *> *) allProductsForCategoryID:(NSInteger) categoryID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rsp.product_id, rsp.product_type_id, rsp.product_name, rsp.product_price,"
                       "rspc.product_category_id, rspc.product_category_name,"
                       "rspt.product_type_id, rspt.product_category_id"
                       " FROM "
                       "rs_product as rsp,"
                       "rs_product_category as rspc,"
                       "rs_product_type as rspt"
                       "rs_product_details as rspd"
                       " WHERE "
                       "rspt.product_category_id = rspc.product_category_id"
                       " AND "
                       "rsp.product_type_id = rspt.product_type_id"
                       " AND "
                       "rspc.product_category_id = ?", @(categoryID)];
    
    NSMutableArray *allProductTypes = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_Product *product  = [RS_Product productFromResultSet:rs];
        
        [allProductTypes addObject:product];
        
    }
    
    [rs close];
    
    return allProductTypes;
}

-(NSArray <RS_Product *> *) allProductsForProductTypeID:(NSInteger) productTypeID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rsp.product_id, rsp.product_type_id, rsp.product_name, rsp.product_price,"
                       "rspt.product_type_id, rspt.product_category_id"
                       " FROM "
                       "rs_product as rsp,"
                       "rs_product_type as rspt"
                       " WHERE "
                       "rsp.product_type_id = rspt.product_type_id"
                       " AND "
                       "rsp.product_type_id = ?", @(productTypeID)];
    
    NSMutableArray *allProductTypes = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_Product *product  = [RS_Product productFromResultSet:rs];
        
        [allProductTypes addObject:product];
        
    }
    
    [rs close];
    
    return allProductTypes;
}


-(RS_Product *) productForProductID:(NSInteger) productID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT "
                       "rsp.product_id, rsp.product_type_id, rsp.product_name, rsp.product_price,"
                       "rspt.product_type_id, rspt.product_category_id,"
                       "rspc.product_category_id, rspc.product_category_name"
                       " FROM "
                       "rs_product as rsp,"
                       "rs_product_category as rspc,"
                       "rs_product_type as rspt"
                       " WHERE "
                       "rspt.product_category_id = rspc.product_category_id"
                       " AND "
                       "rsp.product_type_id = rspt.product_type_id"
                       " AND "
                       "rsp.product_id = ?", @(productID)];
    
    
    RS_Product *product = nil;
    while ([rs next]) {
        
        product  = [RS_Product productFromResultSet:rs];
        
    }
    
    [rs close];
    
    return product;
}



@end
