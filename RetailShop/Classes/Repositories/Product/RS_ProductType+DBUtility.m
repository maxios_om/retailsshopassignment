//
//  RS_ProductType+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductType+DBUtility.h"
#import "AbstractRepository.h"
#import "RS_Category+DBUtility.h"

@implementation RS_ProductType (DBUtility)

+(RS_ProductType *) productTypeFromResultSet:(FMResultSet *)resultSet {
    
    RS_ProductType *productType = [RS_ProductType new];
    productType.productTypeID = [resultSet intForColumn:@"product_type_id"];
    
    productType.category = [RS_Category categoryFromResultSet:resultSet];
    
    return productType;
}

@end
