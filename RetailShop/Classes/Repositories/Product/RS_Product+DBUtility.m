//
//  RS_Product+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_Product+DBUtility.h"
#import "RS_ProductTypeRepository.h"
#import "RS_ProductType+DBUtility.h"

@implementation RS_Product (DBUtility)

+(RS_Product *) productFromResultSet:(FMResultSet *) resultSet {
    
    RS_Product *product = [RS_Product new];
    product.productID = [resultSet intForColumn:@"product_id"];
    product.productName = [resultSet stringForColumn:@"product_name"];
    product.productPrice = [[NSDecimalNumber alloc] initWithDouble:[resultSet doubleForColumn:@"product_price"]];
    
    product.type = [RS_ProductType productTypeFromResultSet:resultSet];
    
    return product;
}

@end
