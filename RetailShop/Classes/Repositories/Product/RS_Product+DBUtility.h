//
//  RS_Product+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_Product.h"
@class FMResultSet;

@interface RS_Product (DBUtility)

+(RS_Product *) productFromResultSet:(FMResultSet *) resultSet;

@end
