//
//  RS_ProductDetailsRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"

@class RS_ProductDetails;

@interface RS_ProductDetailsRepository : AbstractRepository

-(RS_ProductDetails *) productDetailsForProductID:(NSInteger) productID;

@end
