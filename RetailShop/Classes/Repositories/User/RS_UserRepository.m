//
//  RS_UserRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_UserRepository.h"
#import "RS_User+DBUtility.h"

@implementation RS_UserRepository

-(BOOL) isRecordExist:(NSString *)primeryKey {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_user WHERE user_email = ?", primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}


-(RS_User *) userWithID:(NSInteger) userID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT user_id, user_name, user_email FROM rs_user WHERE user_id = ? ", @(userID)];
    
    RS_User *user = nil;
    
    while ([rs next]) {
        
        user  = [RS_User userFromResultSet:rs];
        break;
        
    }
    
    [rs close];
    
    return user;
}


-(RS_User *) userWithEmail:(NSString *) emailID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT user_id, user_name, user_email FROM rs_user WHERE user_email = ? ", emailID];
    
    RS_User *user = nil;
    
    while ([rs next]) {
        
        user  = [RS_User userFromResultSet:rs];
        break;
        
    }
    
    [rs close];
    
    return user;
}


-(BOOL) addUser:(RS_User *) user {
    
#ifdef DUMMY_USER_ENABLED
    
    return [db executeUpdate:@"INSERT INTO rs_user (user_id, user_name, user_email) VALUES (?, ?, ?)", @(user.userID), user.userName, user.userEmail];
    
#else
    
    return [db executeUpdate:@"INSERT INTO rs_user (user_name, user_email) VALUES (?, ?)", user.userName, user.userEmail];
    
#endif
    
    
}

-(BOOL) updateUser:(RS_User *) user {
    
    return [db executeUpdate:@"UPDATE rs_user SET user_name = ?, user_email = ? WHERE user_id = ?", user.userName, user.userEmail, @(user.userID)];
    
}

@end
