//
//  RS_User+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_User+DBUtility.h"
#import "AbstractRepository.h"

@implementation RS_User (DBUtility)

+(RS_User *) userFromResultSet:(FMResultSet *) resultSet {

    RS_User *user = [RS_User new];
    
    user.userID = [resultSet intForColumn:@"user_id"];
    user.userName = [resultSet stringForColumn:@"user_name"];
    user.userEmail = [resultSet stringForColumn:@"user_email"];
    
    return user;
}

@end
