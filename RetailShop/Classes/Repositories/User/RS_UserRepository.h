//
//  RS_UserRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"

@class RS_User;

@interface RS_UserRepository : AbstractRepository

-(RS_User *) userWithID:(NSInteger) userID;

-(RS_User *) userWithEmail:(NSString *) emailID;

-(BOOL) addUser:(RS_User *) user;

-(BOOL) updateUser:(RS_User *) user;

@end
