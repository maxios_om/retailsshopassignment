//
//  RS_User+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_User.h"
@class FMResultSet;

@interface RS_User (DBUtility)

+(RS_User *) userFromResultSet:(FMResultSet *) resultSet;

@end
