//
//  RS_CategoryRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_CategoryRepository.h"
#import "RS_Category.h"
#import "RS_Category+DBUtility.h"

@implementation RS_CategoryRepository

-(BOOL) isRecordExist:(NSString *)primeryKey {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_product_category WHERE product_category_id = ?", primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}


-(NSArray <RS_Category *> *) allCategories {
    
    FMResultSet *rs = [db executeQuery:@"SELECT product_category_id, product_category_name FROM rs_product_category"];
    
    NSMutableArray *allCategories = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_Category *category  = [RS_Category categoryFromResultSet:rs];
        
        [allCategories addObject:category];
        
    }
    
    [rs close];
    
    return allCategories;
}

-(RS_Category *) categoryForCategoryID:(NSInteger) categoryID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT product_category_id, product_category_name FROM rs_product_category WHERE product_category_id = ?", @(categoryID)];
    
    
    RS_Category *category = nil;
    while ([rs next]) {
        
        category  = [RS_Category categoryFromResultSet:rs];
        
    }
    
    [rs close];
    
    return category;
}

@end
