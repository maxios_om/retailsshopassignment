//
//  RS_Category+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_Category+DBUtility.h"
#import "AbstractRepository.h"

@implementation RS_Category (DBUtility)

+(RS_Category *) categoryFromResultSet:(FMResultSet *) resultSet {
    
    RS_Category * category = [RS_Category new];
    
    category.categoryID = [resultSet intForColumn:@"product_category_id"];
    category.categoryName = [resultSet stringForColumn:@"product_category_name"];
    
    return category;
}

@end
