//
//  RS_CategoryRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"
@class RS_Category;

@interface RS_CategoryRepository : AbstractRepository

-(NSArray <RS_Category *> *) allCategories ;
-(RS_Category *) categoryForCategoryID:(NSInteger) categoryID;

@end
