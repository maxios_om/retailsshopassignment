//
//  RS_Category+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_Category.h"
@class FMResultSet;

/**
 *  Convenicane methods to populate instance from resultset
 */
@interface RS_Category (DBUtility)

+(RS_Category *) categoryFromResultSet:(FMResultSet *) resultSet;

@end
