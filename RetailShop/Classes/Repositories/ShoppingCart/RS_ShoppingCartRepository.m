//
//  RS_ShoppingCartRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCartRepository.h"
#import "RS_ShoppingCart+DBUtility.h"
#import "RS_User.h"

@implementation RS_ShoppingCartRepository

-(BOOL) isRecordExist:(NSString *)primeryKey {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_shopping_cart WHERE shopping_cart_status = ? AND shopping_cart_user_id = ?", @(NO),primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}



-(RS_ShoppingCart *) activeShoppingCartForUser:(NSInteger) userID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT shopping_cart_id, shopping_cart_user_id, shopping_cart_status FROM rs_shopping_cart WHERE shopping_cart_status = ? AND shopping_cart_user_id = ?", @(0),@(userID)];
    
    RS_ShoppingCart *shoppingCart = nil;
    
    while ([rs next]) {
        
        shoppingCart  = [RS_ShoppingCart shoppingCartFromResultSet:rs];
        break;
        
    }
    
    [rs close];
    
    return shoppingCart;
}

-(BOOL) addShoppingCart:(RS_ShoppingCart *) cart {
    
    BOOL status = [db executeUpdate:@"INSERT INTO rs_shopping_cart (shopping_cart_user_id, shopping_cart_status) VALUES (?, ?)", @(cart.user.userID), @(cart.cartStatus)];
    
    if (status) {
        
        cart.cartID = [self lastInsertRowId];
    }
    
    return status;
    
}

-(BOOL) updateShoppingCart:(RS_ShoppingCart *) cart {
    
    return [db executeUpdate:@"UPDATE rs_shopping_cart SET shopping_cart_user_id = ?, shopping_cart_status = ? WHERE shopping_cart_id = ?", @(cart.user.userID), @(cart.cartStatus), @(cart.cartID)];
    
}

@end
