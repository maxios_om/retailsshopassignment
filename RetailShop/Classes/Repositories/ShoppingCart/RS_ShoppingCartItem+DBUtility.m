//
//  RS_ShoppingCartItem+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCartItem+DBUtility.h"
#import "AbstractRepository.h"
#import "RS_Product.h"

@implementation RS_ShoppingCartItem (DBUtility)

+(RS_ShoppingCartItem *) shoppingCartItemFromResultSet:(FMResultSet *) resultSet {
    
    RS_ShoppingCartItem *item = [RS_ShoppingCartItem new];
    
    item.itemId = [resultSet intForColumn:@"rs_shopping_cart_item_id"];
    item.cartID = [resultSet intForColumn:@"rs_shopping_cart_id"];
    NSInteger productID = [resultSet intForColumn:@"rs_product_id"];
    item.product = [RS_Product productForProductID:productID];
    item.product.quantity = [resultSet intForColumn:@"quatity"];
    
    return item;
}

@end
