//
//  RS_ShoppingCartItemRepository.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCartItemRepository.h"
#import "RS_ShoppingCartItem+DBUtility.h"
#import "RS_Product.h"

@implementation RS_ShoppingCartItemRepository

-(BOOL) isRecordExist:(NSString *)primeryKey {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_shopping_cart_item WHERE rs_shopping_cart_item_id = ?", primeryKey];
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}

-(BOOL) isExistingItem:(RS_ShoppingCartItem *) item {
    
    int count = [db intForQuery:@"SELECT COUNT (*) FROM rs_shopping_cart_item WHERE rs_shopping_cart_id = ? AND rs_product_id = ? ", @(item.cartID), @(item.product.productID)];
    
    if (count > 0) {
        
        return YES;
    }
    
    return NO;
}

-(NSArray <RS_ShoppingCartItem *> *) allProductsForCart:(NSInteger) cartID {
    
    FMResultSet *rs = [db executeQuery:@"SELECT rs_shopping_cart_item_id, rs_shopping_cart_id, rs_product_id, quatity FROM rs_shopping_cart_item WHERE rs_shopping_cart_id = ?", @(cartID)];
    
    NSMutableArray *items = [NSMutableArray array];
    
    while ([rs next]) {
        
        RS_ShoppingCartItem *item  = [RS_ShoppingCartItem shoppingCartItemFromResultSet:rs];
        
        [items addObject:item];
        
    }

    
    [rs close];
    
    return items;
}

-(BOOL) addShoppingCartItem:(RS_ShoppingCartItem *) cartItem {
        
    BOOL status = [db executeUpdate:@"INSERT INTO rs_shopping_cart_item (rs_shopping_cart_id, rs_product_id, quatity) VALUES (?, ?, ?)", @(cartItem.cartID), @(cartItem.product.productID), @(cartItem.product.quantity)];
    
    if (status) {
        cartItem.itemId = [self lastInsertRowId];
    }
    
    return status;
    
}

-(BOOL) updateShoppingCartItem:(RS_ShoppingCartItem *) cartItem {
    
    return [db executeUpdate:@"UPDATE rs_shopping_cart_item SET rs_shopping_cart_id = ?, rs_product_id = ?, quatity = ? WHERE rs_shopping_cart_id = ? AND rs_product_id = ? ", @(cartItem.cartID), @(cartItem.product.productID), @(cartItem.product.quantity), @(cartItem.cartID), @(cartItem.product.productID)];
    
}


-(BOOL) deleteItem:(RS_ShoppingCartItem *) item {
    
    BOOL status = [db executeUpdate:@"DELETE FROM rs_shopping_cart_item WHERE rs_shopping_cart_id = ? AND rs_product_id = ? ", @(item.cartID), @(item.product.productID)];
    
    return status;
    
}

@end
