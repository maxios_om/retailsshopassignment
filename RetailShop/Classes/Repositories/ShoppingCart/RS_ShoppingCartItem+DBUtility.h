//
//  RS_ShoppingCartItem+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCartItem.h"

@class FMResultSet;

@interface RS_ShoppingCartItem (DBUtility)

+(RS_ShoppingCartItem *) shoppingCartItemFromResultSet:(FMResultSet *) resultSet;

@end
