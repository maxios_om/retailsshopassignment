//
//  RS_ShoppingCart+DBUtility.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCart+DBUtility.h"
#import "AbstractRepository.h"


@implementation RS_ShoppingCart (DBUtility)

+(RS_ShoppingCart *) shoppingCartFromResultSet:(FMResultSet *) resultSet {

    RS_ShoppingCart *cart = [RS_ShoppingCart new];
    cart.cartID = [resultSet intForColumn:@"shopping_cart_id"];
    return cart;
}

@end
