//
//  RS_ShoppingCartItemRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"

@class RS_ShoppingCartItem;

@interface RS_ShoppingCartItemRepository : AbstractRepository

-(BOOL) isExistingItem:(RS_ShoppingCartItem *) item;

-(NSArray <RS_ShoppingCartItem *> *) allProductsForCart:(NSInteger) cartID;

-(BOOL) addShoppingCartItem:(RS_ShoppingCartItem *) cartItem;

-(BOOL) updateShoppingCartItem:(RS_ShoppingCartItem *) cartItem;

-(BOOL) deleteItem:(RS_ShoppingCartItem *) item;

@end
