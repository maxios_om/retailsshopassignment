//
//  RS_ShoppingCart+DBUtility.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCart.h"

@class FMResultSet;

@interface RS_ShoppingCart (DBUtility)

+(RS_ShoppingCart *) shoppingCartFromResultSet:(FMResultSet *) resultSet;

@end
