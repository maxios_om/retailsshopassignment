//
//  RS_ShoppingCartRepository.h
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "AbstractRepository.h"

@class RS_ShoppingCart;

@interface RS_ShoppingCartRepository : AbstractRepository

-(RS_ShoppingCart *) activeShoppingCartForUser:(NSInteger) userID;

-(BOOL) addShoppingCart:(RS_ShoppingCart *) cart;

-(BOOL) updateShoppingCart:(RS_ShoppingCart *) cart;

@end
