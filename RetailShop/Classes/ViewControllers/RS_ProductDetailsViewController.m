//
//  RS_ProductDetailsViewController.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ProductDetailsViewController.h"
#import "RS_Product.h"
#import "RS_ProductDetails.h"
#import "RS_Category.h"
#import "RS_ProductType.h"

@interface RS_ProductDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLable;
@property (weak, nonatomic) IBOutlet UIView *bottomBar;

@end

@implementation RS_ProductDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UI Configrations

-(BOOL) shouldDisplayCartButton {
    
    if (self.productDetailsViewControllerFrom == RS_ProductDetailsViewControllerFromProductList) {
        return YES;
    }
    
    return NO;
    
}

-(void) configureUI {
 
    UIImage *productImage = [UIImage imageNamed:self.product.details.mainImageName];
    
    [self.productImageView setImage:productImage];
    
    [self.productNameInfoLabel setText:self.product.productName];

    [self.productPriceLable setText:[self.product localisedPrice]];
    [self customiseNavigationBackButton];
    
    [self.bottomBar.layer setBorderWidth:1];
    [self.bottomBar.layer setBorderColor:[[UIColor colorWithRed:0.188 green:0.514 blue:0.784 alpha:0.900] CGColor]];
    [self.bottomBar setBackgroundColor:[UIColor whiteColor]];
    
    [self setTitle:self.product.productName];
}

#pragma mark - actions 

-(IBAction) addToCartTapped:(id)sender {
    
    
    BOOL status = [[self currentSession] addProductToCart:self.product];
    
    if (status) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.product.productName message:@"Product is added in cart" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}
@end
