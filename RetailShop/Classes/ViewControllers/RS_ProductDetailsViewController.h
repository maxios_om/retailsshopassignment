//
//  RS_ProductDetailsViewController.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_BaseViewController.h"

@class RS_Product;

/**
 *  From which flow details are loaded from
 */
typedef NS_ENUM(NSInteger, RS_ProductDetailsViewControllerFrom) {
    /**
     *  Product list
     */
    RS_ProductDetailsViewControllerFromProductList = 0,
    /**
     *  Cart
     */
    RS_ProductDetailsViewControllerFromCart,
};


/**
 *  Product Details
 */

@interface RS_ProductDetailsViewController : RS_BaseViewController

/**
 *  Product which details should be displayed
 */
@property (nonatomic , strong) RS_Product *product;

/**
 *  from which flow if from cart it will hide cart button
 */
@property (nonatomic , assign) RS_ProductDetailsViewControllerFrom productDetailsViewControllerFrom;

@end
