//
//  RAS_BaseViewController.h
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RS_UserSessionManager.h"

/**
 *  abstraction layer protocall
 */
@protocol RS_BaseViewController <NSObject>

@optional

+(instancetype)instanceFromStoryBoard;

-(BOOL) shouldDisplayCartButton;

-(void) customiseNavigationBackButton;

@end

/**
 *  user shopping session specific abstraction
 */

@protocol RS_UserSessionManagerCartUpdate <NSObject>

-(void) handleCartUpdate;

@end

/**
 *  parent class for all view controllers
 */

@interface RS_BaseViewController : UIViewController <RS_BaseViewController , RS_UserSessionManagerCartUpdate>

-(RS_UserSessionManager *) currentSession;

@end

