//
//  RS_ShoppingCartViewController.m
//  RetailShop
//
//  Created by Omkar khedekar on 17/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_ShoppingCartViewController.h"
#import "RS_ProductDetailsViewController.h"
#import "RS_ProductTableViewCell.h"
#import "UIView+OK_NIBUtils.h"
#import "RS_ShoppingCart.h"
#import "RS_Product.h"

NSString *const CartToProductDetailsSegueID = @"CartToProductDetails";

@interface RS_ShoppingCartViewController () <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *cartProductsTableView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIView *emptyCartPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *mainContainer;
@property (nonatomic , strong) RS_Product *productToRemove;

@end

@implementation RS_ShoppingCartViewController


-(NSArray *)productsToDisplay {
    NSArray *productsToDisplay = [self.currentSession.activeCart productsInCart];
    return productsToDisplay;
}

#pragma mark - life cycle

-(BOOL) shouldDisplayCartButton {
    
    return NO;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self handleCartUpdate];
    [self configureUI];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureTotalCostLabel {
    NSMutableAttributedString *totalCost = [[NSMutableAttributedString alloc] init];
    
    NSAttributedString *subtotalTitle = [[NSAttributedString alloc] initWithString:@"Subtotal: " attributes:@{
                                                                                                              NSForegroundColorAttributeName:[UIColor colorWithRed:0.213 green:0.606 blue:0.933 alpha:1.000],
                                                                                                              NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:16]
                                                                                                              }];
    [totalCost appendAttributedString:subtotalTitle];
    
    
    NSAttributedString *total = [[NSAttributedString alloc] initWithString:self.currentSession.activeCart.localisedTotalCartPrice attributes:@{
                                                                                                                                               NSForegroundColorAttributeName:[UIColor colorWithRed:0.151 green:0.429 blue:0.660 alpha:1.000],
                                                                                                                                               NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:14]
                                                                                                                                               }];
    
    
    [totalCost appendAttributedString:total];
    
    [self.totalLabel setAttributedText:totalCost];
}

-(void) configureUI {

    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(dismissCartController)];
    [self.navigationItem setRightBarButtonItem:closeButton];
    
    [self registerTableViewCells];
    
    
    [self configureTotalCostLabel];
    
}

-(void) registerTableViewCells {
    UINib * productTableViewCellNib = [UIView ok_nibForClass:[RS_ProductTableViewCell class]];
    [self.cartProductsTableView registerNib:productTableViewCellNib forCellReuseIdentifier:NSStringFromClass([RS_ProductTableViewCell class])];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self productsToDisplay] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RS_ProductTableViewCell *cell = (RS_ProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RS_ProductTableViewCell class]) forIndexPath:indexPath];
    
    NSArray *productsToDisplay = [self productsToDisplay];
    
    RS_Product *product = productsToDisplay[indexPath.row];
    
    [cell populateWithProduct:product andCellType:RS_ProductTableViewCellTypeCartItemCell];
    
    [cell addRemoveProductCallBack:^(RS_Product *product) {
        
        self.productToRemove = product;
        
        NSString *message = @"";
        NSString *title = @"";
        NSString *otherButtonTitle = @"";
        
        if (product.quantity == 1) {
            message = @"Are you sure you want to remove this item?";
            title = @"Remove Item";
            otherButtonTitle = @"Remove";
            
        } else {
            message = @"Are you sure to reduce quatity by 1?";
            title = @"Edit Item";
            otherButtonTitle = @"Remove";
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:otherButtonTitle, nil];
        [alert show];

    }];
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}



#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *productsToDisplay = [[[self currentSession] activeCart] productsInCart];
    RS_Product *product = productsToDisplay[indexPath.row];
    
    [self performSegueWithIdentifier:CartToProductDetailsSegueID sender:product];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:CartToProductDetailsSegueID] && [RS_Product isValidProduct:sender]) {
        
        RS_ProductDetailsViewController *destinationViewController = [segue destinationViewController];
        [destinationViewController setProduct:sender];
        [destinationViewController setProductDetailsViewControllerFrom:RS_ProductDetailsViewControllerFromCart];
    }
    
}

-(void) dismissCartController {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)checkoutButtonTapped:(id)sender {
}

#pragma mark - RS_

-(void) handleCartUpdate {
    [self configureTotalCostLabel];
    if (self.currentSession.activeCart.isEmpty) {
        [self.mainContainer setHidden:YES];
        [self.emptyCartPlaceholder setHidden:NO];
        
    } else {
        
        [self.mainContainer setHidden:NO];
        [self.emptyCartPlaceholder setHidden:YES];
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex != alertView.cancelButtonIndex) {
        BOOL status  = [self.currentSession removeProductFromCart:self.productToRemove];
        if (status) {
            [self.cartProductsTableView reloadData];
        }
    }
}

@end
