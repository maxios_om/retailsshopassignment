//
//  ViewController.h
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_BaseViewController.h"

/**
 *  landing view controller
 */
@interface RS_HomeViewController : RS_BaseViewController


@end

