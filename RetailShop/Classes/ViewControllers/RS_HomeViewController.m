//
//  ViewController.m
//  RetailShop
//
//  Created by Omkar khedekar on 15/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_HomeViewController.h"
#import "RS_ProductTableViewCell.h"
#import "UIView+OK_NIBUtils.h"
#import "RS_Product.h"
#import "NSArray+OK_Utils.h"
#import "RS_ProductDetailsViewController.h"


NSString *const ProductDetailsSegueID = @"ProductDetailsSegue";

@interface RS_HomeViewController () <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *productsTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *categoriesSegmentControl;
@property (nonatomic , copy) NSArray <RS_Product*> *products;

@end

@implementation RS_HomeViewController


-(NSArray <RS_Product*> *) products {
    
    //
    if (!_products) {
        _products = [RS_Product allProducts];
    }
    
    return _products;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureUI];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Configrations 

-(void) configureUI {
    
    [self registerTableViewCells];
}

-(void) registerTableViewCells {
    UINib * productTableViewCellNib = [UIView ok_nibForClass:[RS_ProductTableViewCell class]];
    [self.productsTableView registerNib:productTableViewCellNib forCellReuseIdentifier:NSStringFromClass([RS_ProductTableViewCell class])];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *productsToDisplay = [self productsForSelectedCategory];
    return productsToDisplay.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RS_ProductTableViewCell *cell = (RS_ProductTableViewCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RS_ProductTableViewCell class]) forIndexPath:indexPath];
    
    NSArray *productsToDisplay = [self productsForSelectedCategory];
    RS_Product *product = productsToDisplay[indexPath.row];
    
    [cell populateWithProduct:product andCellType:RS_ProductTableViewCellTypeProductCell];
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}



#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *productsToDisplay = [self productsForSelectedCategory];
    RS_Product *product = productsToDisplay[indexPath.row];
    
    [self performSegueWithIdentifier:ProductDetailsSegueID sender:product];
    
}

#pragma mark - Actions
- (IBAction)categoryDidChanged:(UISegmentedControl *)sender {
    [self.productsTableView reloadData];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:ProductDetailsSegueID] && [RS_Product isValidProduct:sender]) {
        
        RS_ProductDetailsViewController *destinationViewController = [segue destinationViewController];
        [destinationViewController setProduct:sender];
    }
     
}


#pragma mark - helpers

-(NSArray *) productsForSelectedCategory {
    
    NSArray *filteredProducts = [NSArray array];
    
    switch (self.categoriesSegmentControl.selectedSegmentIndex) {
            
        case 0: // All
        {
            filteredProducts = [self products];
        }
            break;
        case 1: // Electronics
        {
            filteredProducts = [self.products ok_filteredArrayWithValue:@(1) forKeyPath:@"type.category.categoryID"];
            
        }
            break;
            
        case 2: // Furniture
        {
            filteredProducts = [self.products ok_filteredArrayWithValue:@(2) forKeyPath:@"type.category.categoryID"];
            
        }
            break;
        default:
            break;
    }
    
    return filteredProducts;
}

@end
