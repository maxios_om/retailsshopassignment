//
//  RAS_BaseViewController.m
//  RetailShop
//
//  Created by Omkar khedekar on 16/01/16.
//  Copyright © 2016 omsfactory. All rights reserved.
//

#import "RS_BaseViewController.h"
#import "RS_ShoppingCart.h"
#import "RS_ShoppingCartViewController.h"

@interface RS_BaseViewController ()

@property (nonatomic , strong) UIBarButtonItem *cartButton;

@end

@implementation RS_BaseViewController


-(UIBarButtonItem *) cartButton {
    
    if (!_cartButton) {
        
        UIBarButtonItem *cartButton = [[UIBarButtonItem alloc]
                                       initWithImage:[UIImage imageNamed:@"cart"]
                                       style:UIBarButtonItemStylePlain
                                       target:self
                                       action:@selector(cartButtonTapped)];
        _cartButton = cartButton;

        
    }
    
    return _cartButton;
    
}


-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if ([self shouldDisplayCartButton]) {
     
        self.navigationItem.rightBarButtonItem = [self cartButton];

    }

    [self.view setBackgroundColor:[UIColor colorWithRed:0.937 green:0.937 blue:0.945 alpha:1.000]];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCartUpdate) name:RS_UserSessionManagerCartDidUpdateNotification object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UI configrations

-(void) customiseNavigationBackButton {

        [self.navigationItem setHidesBackButton:YES];
        
        self.navigationItem.leftBarButtonItem = ({
            UIImage *menuIcon = [UIImage imageNamed:@"back"];
            menuIcon = [menuIcon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:menuIcon style:UIBarButtonItemStylePlain target:self action:@selector(popViewController)];
            item;
            
        });
}


-(void) popViewController {
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - session

-(RS_UserSessionManager *) currentSession {
    
    return [RS_UserSessionManager sharedManager];
}

#pragma mark - RS_BaseViewController should be overriden by subclass as required 

-(BOOL) shouldDisplayCartButton {
    
    return YES;
}

+(instancetype)instanceFromStoryBoard {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyBoard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}



#pragma mark - cart button action
-(void)cartButtonTapped {
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *shoppingCartNavigationController = [storyBoard instantiateViewControllerWithIdentifier:@"MyCartNavigationController"];
    [self presentViewController:shoppingCartNavigationController animated:YES completion:nil];
    
}


#pragma makr - cart updates 

-(void) handleCartUpdate {

    
}

#pragma mark - cleanup 

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
